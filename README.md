# Example of Customized Composer Library

This project is an example of creating a composer library. This is *not* a *private* repository.

### Host your library in Github / Bitbucket

```
git init
git remote add origin [Repo URL]
git add -A
git commit -am "commit message"
git push --set-upstream origin master
```

### Create a composer.json on your library's folder

This is the complete composer.json file

```
{
    "name": "kendrickrmit/package-test",
    "description": "A very useless library made for testing",
    "license": "MIT",
    "autoload": {
        "psr-0" : {
            "Kendrickrmit\\PackageTest" : "src"
        }
    },
    "minimum-stability": "stable",
    "repositories": [
        {
            "type": "vcs",
            "url": "https://bitbucket.org/kendrick_pp/package-test"
        }
    ],
    "extra": {
        "branch-alias": {
            "dev-master": "1.0.x-dev"
        }
    }
}
```

##### Explanations

This example will use PSR-0 as an autoload standard. There are several other standards, but I haven't got the chance to experiment with them.

```
"autoload": {
	"psr-0" : {
		"Kendrickrmit\\PackageTest" : "src"
	}
},
```

repositories will hold the detail of your library on Github/Bitbucket

```
"repositories": [
	{
		"type": "vcs",
		"url": "https://bitbucket.org/kendrick_pp/package-test"
	}
],
```

The extra attributes hold the version number of your library

```
"extra": {
	"branch-alias": {
		"dev-master": "1.0.x-dev"
	}
}
```
use `composer validate` to validate your composer.json file

### Stable release

If you don't provide releases, you will have to add `"minimum-stability": "dev",` on the root of your *project's* composer.json. Otherwise, composer won't find any packages because it requires stable release by default.

To create a release, use a git command

```
git tag 1.0.0
git push --tag
```

### Register your library to packagist

Go to [https://packagist.org/] and register an account.

Submit your package by providing a public url to your repository [https://packagist.org/packages/submit]

e.g. [https://bitbucket.org/kendrick_pp/package-test.git]

Please wait because packagist needs some time to crawl into your repository.